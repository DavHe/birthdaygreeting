Create Database MemberDB
GO
Use MemberDB
GO
Create Table Member (
  FirstName varchar(20),
  LastName varchar(20),
  Gender varchar(6),
  BirthDay smalldatetime,
  Email varchar(40)
)
GO

Insert Into Member values ('Robert', 'Yen', 'Male', '1985/8/8', 'robert.yen@linecorp.com')
Insert Into Member values ('Cid', 'Change', 'Male', '1990/10/10', 'cid.change@linecorp.com')
Insert Into Member values ('Miki', 'Lai', 'Female', '1993/4/5', 'miki.lai@linecorp.com')
Insert Into Member values ('Sherry', 'Chen', 'Female', '1993/8/8', 'sherry.lai@linecorp.com')
Insert Into Member values ('Peter', 'Wang', 'Male', '1950/12/22', 'peter.wang@linecorp.com')

GO